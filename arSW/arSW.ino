/*
- Arduino Serial Echo Server -
 Cada byte recebido e reenvidado pela serial. Ainda durante a recepço
 de um byte o LED ligado ao pino 13 pisca.
 */

int currentAnimation;
int countAnim0, countAnim1, countAnim2;

void setup() {
  /* Inicializa a porta serial em 9600 bytes/s */
  Serial.begin(9600);
  /* Configura o pino de IO para piscar o led durante a recepção. */
  pinMode(13, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(9, OUTPUT);

  pinMode(3, OUTPUT);
  pinMode(2, OUTPUT);
  pinMode(1, OUTPUT);
  currentAnimation = 0;
  countAnim0 = 0;
  countAnim1 = 1;
  countAnim2 = 2;

}
void animation1(){
  digitalWrite(13, HIGH);
  digitalWrite(12, HIGH);
  digitalWrite(11, HIGH);
  digitalWrite(10, HIGH);
  digitalWrite(9, HIGH);
  delay(100);
  digitalWrite(13, LOW);
  digitalWrite(12, LOW);
  digitalWrite(11, LOW);
  digitalWrite(10, LOW);
  digitalWrite(9, LOW);
  delay(100);
}
void animation0(){
  for (int i=13; i >= 9; i--){
    digitalWrite(13, LOW);
    digitalWrite(12, LOW);
    digitalWrite(11, LOW);
    digitalWrite(10, LOW);
    digitalWrite(9, LOW);
    digitalWrite(i, HIGH);
    delay(200);
  } 
}
void animation2(){
  for (int i=9; i <= 13; i++){
    digitalWrite(13, LOW);
    digitalWrite(12, LOW);
    digitalWrite(11, LOW);
    digitalWrite(10, LOW);
    digitalWrite(9, LOW);
    digitalWrite(i, HIGH);
    delay(200);
  } 
}

void animationCount(){
  if (currentAnimation == 0)
    countAnim0++;
  else if (currentAnimation == 1)
    countAnim1++;
  else if (currentAnimation == 2)
    countAnim2++;
}

void loop() {
  if (currentAnimation == 0){
    animation0();
  }
  if (currentAnimation == 1)
    animation1();
  if (currentAnimation == 2)
    animation2();
    
  /* Aguarda o recebimento de dados */ 
  if (Serial.available()) {
    /* Le byte da serial */
    int inByte = Serial.read();
    if (inByte == 1){
      if ( currentAnimation < 2)
        currentAnimation++;
      else 
        currentAnimation = 0;
      animationCount();
    } 
    else if (inByte == 2){
      Serial.write(countAnim0);
      Serial.write(countAnim1);
      Serial.write(countAnim2);
    } 
    else if (inByte == 3){
      if (currentAnimation > 0)
        currentAnimation--;
      else
        currentAnimation = 2;
      animationCount();
    }
    delay(10);
  }
}











