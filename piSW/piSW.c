#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <iostream>
using namespace std;
 
#define IN  0
#define OUT 1
 
#define LOW  0
#define HIGH 1
 
static int
GPIOExport(int pin)
{
        #define BUFFER_MAX 3
	char buffer[BUFFER_MAX];
	ssize_t bytes_written;
	int fd;
 
	fd = open("/sys/class/gpio/export", O_WRONLY);
	if (-1 == fd) {
		fprintf(stderr, "Failed to open export for writing!\n");
		return(-1);
	}
 
	bytes_written = snprintf(buffer, BUFFER_MAX, "%d", pin);
	write(fd, buffer, bytes_written);
	close(fd);
	return(0);
}
 
static int
GPIOUnexport(int pin)
{
	char buffer[BUFFER_MAX];
	ssize_t bytes_written;
	int fd;
 
	fd = open("/sys/class/gpio/unexport", O_WRONLY);
	if (-1 == fd) {
		fprintf(stderr, "Failed to open unexport for writing!\n");
		return(-1);
	}
 
	bytes_written = snprintf(buffer, BUFFER_MAX, "%d", pin);
	write(fd, buffer, bytes_written);
	close(fd);
	return(0);
}
 
static int
GPIODirection(int pin, int dir)
{
	static const char s_directions_str[]  = "in\0out";
 
        #define DIRECTION_MAX 35
	char path[DIRECTION_MAX];
	int fd;
 
	snprintf(path, DIRECTION_MAX, "/sys/class/gpio/gpio%d/direction", pin);
	fd = open(path, O_WRONLY);
	if (-1 == fd) {
		fprintf(stderr, "Failed to open gpio direction for writing!\n");
		return(-1);
	}
 
	if (-1 == write(fd, &s_directions_str[IN == dir ? 0 : 3], IN == dir ? 2 : 3)) {
		fprintf(stderr, "Failed to set direction!\n");
		return(-1);
	}
 
	close(fd);
	return(0);
}
 
static int
GPIORead(int pin)
{
        #define VALUE_MAX 30
	char path[VALUE_MAX];
	char value_str[3];
	int fd;
 
	snprintf(path, VALUE_MAX, "/sys/class/gpio/gpio%d/value", pin);
	fd = open(path, O_RDONLY);
	if (-1 == fd) {
		fprintf(stderr, "Failed to open gpio value for reading!\n");
		return(-1);
	}
 
	if (-1 == read(fd, value_str, 3)) {
		fprintf(stderr, "Failed to read value!\n");
		return(-1);
	}
 
	close(fd);
 
	return(atoi(value_str));
}
 
static int
GPIOWrite(int pin, int value)
{
	static const char s_values_str[] = "01";
 
	char path[VALUE_MAX];
	int fd;
 
	snprintf(path, VALUE_MAX, "/sys/class/gpio/gpio%d/value", pin);
	fd = open(path, O_WRONLY);
	if (-1 == fd) {
		fprintf(stderr, "Failed to open gpio value for writing!\n");
		return(-1);
	}
 
	if (1 != write(fd, &s_values_str[LOW == value ? 0 : 1], 1)) {
		fprintf(stderr, "Failed to write value!\n");
		return(-1);
	}
 
	close(fd);
	return(0);
}

int readBtn(int btn){
    /*
     * Set GPIO directions
     */

    if (-1 == GPIODirection(btn, OUT))
      return(2);
    /*
     * Write GPIO value
     */
    if (-1 == GPIOWrite(btn, 1))
      return(3);
 
    /*
     * Set GPIO directions
     */
    if (-1 == GPIODirection(btn, IN))
      return(2);

    /*
     * Read GPIO value
     */
    int readValue = GPIORead(btn);
    printf("I'm reading %d in GPIO %d\n", readValue, btn);
    return readValue;
} 
 
int
main(int argc, char *argv[])
{
  int repeat = 10000;
//  int pushbutton;
  int btn1 = 22;
  int btn2 = 23;
  int btn3 = 24;

/* cout << "Insert the gpio pin related to the pushbutton: ";
 * cin >> pushbutton;
 * cout << endl;    
 */
  /*
   * Enable GPIO pins
   */
 
  if (-1 == GPIOExport(btn1) || -1 == GPIOExport(btn2) || -1 == GPIOExport(btn3))
    return(1);

  do {
    readBtn(btn1);
    readBtn(btn2);
    readBtn(btn3);
    usleep(500 * 1000);
  }
  while (repeat--);
 
  /*
   * Disable GPIO pins
   */
  if (-1 == GPIOUnexport(btn1) || -1 == GPIOUnexport(btn2) || -1 == GPIOUnexport(btn3))
    return(4);
 
  return(0);
}

